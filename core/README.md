## 1. create and activate venv

`python3 -m venv some_venv`
`source venv/bin/activate`

## 2. install packages

`pip install --upgrade pip` <br>
`pip install -r ./~requirements/dev_freeze.txt`

## 3. run docker postgresql db and pgadmin in docker from dev-docker dir

`docker compose up`

## 4. set db settings for django DATABASE_URL in .env
## 4.1 db name should be the same as that is in the docker compose file
## 4.2 .env.dev file contains DATABASE_URL setting, don't forgret change it

DATABASE_URL=postgres://postgres:postgres@localhost:5432/postgres_db

## 5. to run pgadmin -> register server, set up next settings

- General -> give some name
- Connection -> host name is docker db container name
- use docker db env variables for 'user' and 'password'

## 6. run django migrations for django admin

`python manage.py migrate`

## 7. create admin user

`python manage.py createsuperuser`

## 8. add fixtures
`python manage.py loaddata category_members_fixture.json` <br>
`python manage.py loaddata categories_fixture.json`
