from django.contrib import admin
from .models import Category, CategoryMember


@admin.register(CategoryMember)
class CategoryMemberAdmin(admin.ModelAdmin):
    list_display = ('name', 'gender', 'email', 'phone_number', 'address')
    list_filter = ('gender', 'hobby')


class CategoryMemberInline(admin.TabularInline):
    model = Category.members.through
    extra = 1


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'creation_date', 'is_active')
    list_filter = ('creation_date', 'is_active')
    exclude = ["members"]
    inlines = [CategoryMemberInline]
