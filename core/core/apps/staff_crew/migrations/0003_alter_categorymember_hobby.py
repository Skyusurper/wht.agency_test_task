# Generated by Django 4.2.6 on 2023-10-23 17:31

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        (
            "staff_crew",
            "0002_alter_category_options_alter_categorymember_options_and_more",
        ),
    ]

    operations = [
        migrations.AlterField(
            model_name="categorymember",
            name="hobby",
            field=models.CharField(
                blank=True,
                choices=[
                    ("Reading", "Reading"),
                    ("Painting", "Painting"),
                    ("Gardening", "Gardening"),
                    ("Cooking", "Cooking"),
                    ("Traveling", "Traveling"),
                    ("Sports", "Sports"),
                    ("Music", "Music"),
                    ("Dancing", "Dancing"),
                    ("Hiking", "Hiking"),
                    ("Photography", "Photography"),
                ],
                max_length=200,
            ),
        ),
    ]
