# Generated by Django 4.2.6 on 2023-10-23 17:02

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("staff_crew", "0001_initial"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="category",
            options={"verbose_name": "Category", "verbose_name_plural": "Categories"},
        ),
        migrations.AlterModelOptions(
            name="categorymember",
            options={
                "verbose_name": "Category member",
                "verbose_name_plural": "Category members",
            },
        ),
        migrations.AlterField(
            model_name="category",
            name="creation_date",
            field=models.DateField(default=datetime.date.today),
        ),
        migrations.AlterField(
            model_name="category",
            name="is_active",
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name="category",
            name="name",
            field=models.CharField(max_length=100),
        ),
    ]
