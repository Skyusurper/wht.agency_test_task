from rest_framework import viewsets
from .models import Category, CategoryMember
from .serializers import CategorySerializer, CategoryMemberSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CategoryMemberViewSet(viewsets.ModelViewSet):
    queryset = CategoryMember.objects.all()
    serializer_class = CategoryMemberSerializer


# add get_queryset, add custom paggination
