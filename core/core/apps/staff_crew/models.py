import datetime

from django.db import models


class CategoryMember(models.Model):
    name = models.CharField(max_length=100)
    gender = models.CharField(
        max_length=10,
        choices=(('Male', 'Male'), ('Female', 'Female'), ('Other', 'Other')),
        blank=True,
    )
    email = models.EmailField(max_length=100, blank=True)
    phone_number = models.CharField(max_length=100, blank=True)
    address = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    HOBBY_CHOICES = (
        ('Reading', 'Reading'),
        ('Painting', 'Painting'),
        ('Gardening', 'Gardening'),
        ('Cooking', 'Cooking'),
        ('Traveling', 'Traveling'),
        ('Sports', 'Sports'),
        ('Music', 'Music'),
        ('Dancing', 'Dancing'),
        ('Hiking', 'Hiking'),
        ('Photography', 'Photography'),
    )

    hobby = models.CharField(max_length=200, choices=HOBBY_CHOICES, blank=True)

    class Meta:
        verbose_name = 'Category member'
        verbose_name_plural = 'Category members'

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    creation_date = models.DateField(default=datetime.date.today)
    is_active = models.BooleanField(default=False)
    image = models.ImageField(
        upload_to='category_images/', null=True, blank=True
    )
    members = models.ManyToManyField(
        CategoryMember, related_name='categories', blank=True
    )

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name
