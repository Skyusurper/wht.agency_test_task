from django.apps import AppConfig


class StuffCrewConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "core.apps.staff_crew"
