from rest_framework import serializers

from .models import Category, CategoryMember


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    members = serializers.HyperlinkedRelatedField(
        many=True, read_only=True, view_name='categorymember-detail'
    )

    class Meta:
        model = Category
        # fields = '__all__'
        fields = [
            'id',
            'name',
            'description',
            'creation_date',
            'is_active',
            'image',
            'members',
        ]


class CategoryMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoryMember
        fields = '__all__'
