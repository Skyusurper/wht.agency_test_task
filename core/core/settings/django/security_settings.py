from ..base import env

# A list of trusted origins for unsafe requests (e.g. POST).
CSRF_TRUSTED_ORIGINS = env.list('CSRF_TRUSTED_ORIGINS', default=[])

# cookie httpOnly flag, defines if client-side JavaScript has access to cookie
CSRF_COOKIE_HTTPONLY = env.bool('CSRF_COOKIE_HTTPONLY', default=False)

# CSRF cookie will be marked as “secure”
CSRF_COOKIE_SECURE = env.bool('CSRF_COOKIE_SECURE', default=False)

# SameSite flag on cookie, prevent from sending in cross-site requests
CSRF_COOKIE_SAMESITE = env.str('CSRF_COOKIE_SAMESITE', default='Lax')

# cookie domain flag, if specified -> subdomains included
CSRF_COOKIE_DOMAIN = env.str('CSRF_COOKIE_DOMAIN', default='')

# define client-side JavaScript access to session cookie
SESSION_COOKIE_HTTPONLY = env.bool('SESSION_COOKIE_HTTPONLY', default=False)

# session cookie will be marked as “secure”
SESSION_COOKIE_SECURE = env.bool('SESSION_COOKIE_SECURE', default=False)

# redirect all non-https to https
SECURE_SSL_REDIRECT = env.bool('SECURE_SSL_REDIRECT', default=False)

# turn on HTTP Strict Transport Security
SECURE_HSTS_SECONDS = env.int('SECURE_HSTS_SECONDS', default=0)
# prohibit http connection, adds the includeSubDomains directive
SECURE_HSTS_INCLUDE_SUBDOMAINS = env.bool(
    'SECURE_HSTS_INCLUDE_SUBDOMAINS', default=False
)
# prohibit browser guess content type
SECURE_CONTENT_TYPE_NOSNIFF = True
# is_secure() method checks if url uses https://
SECURE_PROXY_SSL_HEADER = env.tuple('SECURE_PROXY_SSL_HEADER', default=None)
