from ..base import env

# origins that are authorized to make cross-site HTTP requests
CORS_ALLOWED_ORIGINS = env.list('CORS_ALLOWED_ORIGINS', default=[])

# the list of extra HTTP headers to expose to the browser
CORS_EXPOSE_HEADERS = env.list('CORS_EXPOSE_HEADERS', default=[])

# define if cookies will be allowed to be included in cross-site HTTP requests
CORS_ALLOW_CREDENTIALS = env.bool('CORS_ALLOW_CREDENTIALS', default=False)
