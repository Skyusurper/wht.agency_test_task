from google.cloud import storage

from ..base import env
from ..django.main import INSTALLED_APPS

INSTALLED_APPS += ["storages"]

# domain name to set origin
DOMAIN_NAME = env.str('SITE_DOMAIN_NAME', default="")

# GOOGLE BUCKET SETTINGS FOR STATIC AND MEDIA FILES
GS_BUCKET_NAME = env.str('GS_BUCKET_NAME', default="")
GS_PATH = env.str('GS_PATH', default="")
GS_MEDIA_DIR = env.str('GS_MEDIA_DIR', default="")
GS_STATIC_DIR = env.str('GS_STATIC_DIR', default="")

# if fine-grained on bucket
# GS_DEFAULT_ACL = "publicRead"

# if GS_DEFAULT_ACL = None
GS_QUERYSTRING_AUTH = False
GS_DEFAULT_ACL = None


# set cross-origin allow header
def cors_configuration(bucket_name):
    """Set a bucket's CORS policies configuration."""
    # bucket_name = "your-bucket-name"

    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    origin = f"https://{DOMAIN_NAME}.com" if DOMAIN_NAME != '' else "*"
    bucket.cors = [
        {
            "origin": [origin],
            "responseHeader": [
                "Content-Type",
                "x-goog-resumable",
                "Access-Control-Allow-Origin",
            ],
            "method": ['POST', 'GET'],
            "maxAgeSeconds": 120,
        }
    ]
    bucket.patch()

    # origin could be https://some_site.com.
    # print("Set CORS policies for bucket {} is {}".format(bucket.name, bucket.cors))
    return bucket


if GS_BUCKET_NAME:
    cors_configuration(GS_BUCKET_NAME)
