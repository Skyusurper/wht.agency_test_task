"""
Modify django-storages to store static and media files in sub-folders in a google cloud storage bucket
"""
from abc import ABC

from storages.backends.gcloud import GoogleCloudStorage

from .gcloud_storage import GS_MEDIA_DIR, GS_STATIC_DIR


class GoogleCloudMediaStorage(GoogleCloudStorage, ABC):
    """
    GoogleCloudStorage for Django's Media files.
    """

    def __init__(self, *args, **kwargs):
        kwargs['location'] = GS_MEDIA_DIR
        super().__init__(*args, **kwargs)


class GoogleCloudStaticStorage(GoogleCloudStorage, ABC):
    """
    GoogleCloudStorage for Django's Static files
    """

    def __init__(self, *args, **kwargs):
        kwargs['location'] = GS_STATIC_DIR
        super().__init__(*args, **kwargs)
