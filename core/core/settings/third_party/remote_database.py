# remote database and storage
# google cloud settings to use cloud sql and cloud storage
# before run local_remote in manage.py, complete next steps:
# export GOOGLE_CLOUD_PROJECT=shot-test-2022-v1; export USE_CLOUD_SQL_AUTH_PROXY=true
# sudo systemctl stop postgresql - stop local server postgresql
# in separate shell run: ./cloud_sql_proxy -instances="shot-test-2022-v1:europe-central2:shop-psql-id-name"=tcp:5432
# and optional
# python manage.py makemigrations
# python manage.py migrate
# python manage.py collectstatic

import io
import os

import environ
import google.auth
from google.cloud import secretmanager

env = environ.Env()

try:
    _, os.environ["GOOGLE_CLOUD_PROJECT"] = google.auth.default()
except google.auth.exceptions.DefaultCredentialsError:
    pass

if os.environ.get("GOOGLE_CLOUD_PROJECT", None):
    project_id = os.environ.get("GOOGLE_CLOUD_PROJECT")
    client = secretmanager.SecretManagerServiceClient()
    settings_name = os.environ.get("SETTINGS_NAME", "shop_secret_settings")
    name = f"projects/{project_id}/secrets/{settings_name}/versions/latest"
    payload = client.access_secret_version(name=name).payload.data.decode("UTF-8")
    env.read_env(io.StringIO(payload))
else:
    raise Exception("No GOOGLE_CLOUD_PROJECT detected. No secrets found.")

DATABASES = {"default": env.db()}

if os.getenv("USE_CLOUD_SQL_AUTH_PROXY", None):
    DATABASES["default"]["HOST"] = "127.0.0.1"
    DATABASES["default"]["PORT"] = 5432
