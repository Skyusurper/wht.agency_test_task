# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.1/howto/static-files/

from .base import BASE_DIR, env
from .third_party import GS_BUCKET_NAME, GS_MEDIA_DIR, GS_PATH, GS_STATIC_DIR


def file_env_var(path: str, bucket: str, folder: str) -> str:
    if path and bucket and folder:
        return f'{path}/{bucket}/{folder}/'
    else:
        return folder + '/'


# media file storage
DEFAULT_FILE_STORAGE = env.str(
    'DEFAULT_FILE_STORAGE',
    default="django.core.files.storage.FileSystemStorage",
)
# static file storage engine to be used by collectstatic command
STATICFILES_STORAGE = env.str(
    'STATICFILES_STORAGE',
    default="django.contrib.staticfiles.storage.StaticFilesStorage",
)

vars = {'path': GS_PATH, 'bucket': GS_BUCKET_NAME}

MEDIA_URL = file_env_var(**vars, folder=GS_MEDIA_DIR)
# URL to use when referring to static files located in STATIC_ROOT
STATIC_URL = file_env_var(**vars, folder=GS_STATIC_DIR)

# absolute filesystem path to the directory that will hold user-uploaded files; dev
MEDIA_ROOT = env.str('MEDIA_ROOT', default=f'{BASE_DIR}/core/media')

# dir absolute path, collectstatic cmd places static files for production here; dev
# e.g. server dir, that is set up in nginx config file
STATIC_ROOT = env.str('STATIC_ROOT', default=f'{BASE_DIR}/core/staticfiles')

# defines the additional locations where finder looks for static files; prod
# STATICFILES_DIRS = [
#     BASE_DIR.parents[1] / 'frontend/front_root/dist/spa',
# ]
