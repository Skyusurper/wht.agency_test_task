import environ
from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
# BASE_DIR is dir where manage.py is located
BASE_DIR = Path(__file__).resolve().parents[2]

# django-environ settings
env = environ.Env()

env_file = Path(BASE_DIR / ".env.dev")

if env_file.exists():
    # print('file exists', env_file)
    env.read_env(env_file)
