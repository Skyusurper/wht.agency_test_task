-r production.txt

# python auto-formatter, styling code
black

# python linter, check code complexity, PEPS, etc.
flake8

# import auto-formatter
isort

# test coverage reports
# coverage

# complex test cases for django models
# factory-boy
# pytest

# interface for CI tools
# tox==3.23.1

# ipython==7.25.0

# django-debug-toolbar==3.2.1

# google-cloud-secret-manager==2.8.0
